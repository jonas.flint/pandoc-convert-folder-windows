import argparse
import os, fnmatch
from pathlib import Path
from subprocess import check_output

parser = argparse.ArgumentParser(description='Convert markdown files to a single PDF with pandoc')
parser.add_argument('-x', '--pandoc', default="pandoc", help="path to pandoc file (default: pandoc)")
parser.add_argument('-f', '--folder', help="folder to take *md files from (default: current folder)")
parser.add_argument('-o', '--output', default="result.pdf", help="name of output file (default: result.pdf")

args = parser.parse_args()

# define pandoc args
pandoc_file_str = args.pandoc
output_file_str = args.output

# get files
folder_path_str = args.folder if args.folder is not None else '.'
folder_path = Path(folder_path_str)
files_list = fnmatch.filter(os.listdir(folder_path_str), '*.md')

# build command
command_str = pandoc_file_str
for file_str in files_list:
  file_path = folder_path.joinpath(file_str)
  file_path_str = str(file_path.resolve())
  command_str = command_str + ' ' + file_path_str
command_str = command_str + ' -o ' + output_file_str

# run command
print('starting pandoc: "$ ' + command_str + '"')
shell_output = check_output(command_str, shell=True).decode()
print (shell_output)