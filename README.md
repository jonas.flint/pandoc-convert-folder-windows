# Pandoc folder convert for Windows

This repository contains python scripts to convert all files of a folder with pandoc in Windows.

## Markdown to PDF

```
$ pandoc-folder-markdown-to-pdf.py --help

usage: pandoc-folder-markdown-to-pdf.py [-h] [-x PANDOC] [-f FOLDER]
                                        [-o OUTPUT]

Convert markdown files to a single PDF with pandoc

optional arguments:
  -h, --help            show this help message and exit
  -x PANDOC, --pandoc PANDOC
                        path to pandoc file (default: pandoc)
  -f FOLDER, --folder FOLDER
                        folder to take *md files from (default: current
                        folder)
  -o OUTPUT, --output OUTPUT
                        name of output file (default: result.pdf
                        
```
